from alpine/git
workdir /src
run ( \
  wget -O- "https://gitlab.com/api/v4/groups/emulation-as-a-service/projects?include_subgroups=true&per_page=100&page=1"; \
  wget -O- "https://gitlab.com/api/v4/groups/emulation-as-a-service/projects?include_subgroups=true&per_page=100&page=2"; \
  wget -O- "https://gitlab.com/api/v4/groups/emulation-as-a-service/projects?include_subgroups=true&per_page=100&page=3"; \
  ) | grep -E '"http_url_to_repo":"[^"]+"' -o | cut -d '"' -f 4 \
  | sed 'h;s/\.git$/.wiki.git/;H;g' \
  | xargs -n 1 git clone --bare || :
